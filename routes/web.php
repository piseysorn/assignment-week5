<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/index', function () {
//     return view('index');
// });
// Route::get('/contact', function () {
//     return view('contact');
// });
// Route::get('/post', function () {
//     return view('post');
// });
// Route::get('/about', function () {
//     return view('about');
// });
Route::get('/pages',[App\Http\Controllers\PageController::class,'index'])->name('pages.index');
Route::get('/pages/auth/login',[App\Http\Controllers\PageController::class,'login'])->name('admins.login');
Route::get('/pages/auth/register',[App\Http\Controllers\PageController::class,'register'])->name ('admins.register');
Route::get('/admins/create',[App\Http\Controllers\DashboardController::class,'create'])->name('admins.create');
Route::get('/admins/{id}/edit',[App\Http\Controllers\DashboardController::class,'edit'])->name('admins.edit');
Route::delete('/admins/{page}/destroy',[App\Http\Controllers\DashboardController::class,'destroy'])->name('admins.destroy');
Route::put('/admins/{page}/update',[App\Http\Controllers\DashboardController::class,'update'])->name('admins.update');
Route::post('/admins/store',[App\Http\Controllers\DashboardController::class,'store'])->name('admins.store');
Route::post('/pages/store',[App\Http\Controllers\PageController::class,'store'])->name('pages.store');
Route::get('/pages/{id}/edit',[App\Http\Controllers\PageController::class,'edit'])->name('pages.edit');
Route::get('/pages/{id}/update',[App\Http\Controllers\PageController::class,'update'])->name('pages.update');
Route::get('/admins',[App\Http\Controllers\DashboardController ::class,'dashboard'])->name('admins.dashboard')->middleware('admin_role');
Route::get('/admins/forms',[App\Http\Controllers\DashboardController ::class,'forms'])->name('admins.forms');
Route::get('/admins/tables_data',[App\Http\Controllers\DashboardController ::class,'tables_data'])->name('admins.tables_data');
Route::get('/admins/login',[App\Http\Controllers\DashboardController ::class,'login'])->name('admins.login');
Route::get('/admins/register',[App\Http\Controllers\DashboardController ::class,'register'])->name('admins.register');

