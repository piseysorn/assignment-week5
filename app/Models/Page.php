<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;
    public $fillable=[
        'title',
        'subtitle',
        'author',
        'publish_date',
        'content'
    ];
}
