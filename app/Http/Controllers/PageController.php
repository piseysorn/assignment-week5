<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;

class PageController extends Controller
{
    public function index()
    {
        $pages = Page::get();
        return view('pages.index',[
            'pages'=>$pages
                ]);
    }
    public function create()
    {
         return view('pages.create');
    }
    public function store(Request $request)
    {
       //dd('it work');
        $page = new Page();
        $page->title=$request->get('title');
        $page->subtitle=$request->get('subtitle');
        $page->author=$request->get('author');
        $page->publish_date=$request->get('publish_date');
        $page->content=$request->get('content');
        $page->save();
        return redirect(route('pages.index'));

    }
    public function show($id)
    {

    }
    public function edit($id)
    {
        $page=Page::find($id);
        return view('pages.edit')->with('page',$page);
    }
    public function update($id)
    {

    }
    public function destroy($id)
    {

    }
    public function forceDestroy($id)
    {

    }
    public function login()
    {
        return view('auth.login');
    }
    public function register()
    {
        return view('auth.register');
    }
}
