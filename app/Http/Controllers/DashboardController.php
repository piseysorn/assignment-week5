<?php

namespace App\Http\Controllers;
use App\Models\Page;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard()
    {
        return view('admins.dashboard');
        
    }
    public function create()
    {
         return view('admins.create');
    }
    public function store(Request $request)
    {
        //dd('it work');
        $page = new Page();
        $page->title=$request->get('title');
        $page->subtitle=$request->get('subtitle');
        $page->author=$request->get('author');
        $page->publish_date=$request->get('publish_date');
        $page->content=$request->get('content');
        $page->save();
        return redirect(route('admins.tables_data'));

    }
    public function edit($id)
    {
        $page=Page::find($id);
        if (empty($page)){
            return redirect(route('admins.tables_data'));
        }
        return view('admins.edit')->with('page',$page);
    }
    public function update(Page $page,Request $request)
    {
       // dd($request->all());
        $page->title=$request->get('title');
        $page->subtitle=$request->get('subtitle');
        $page->author=$request->get('author');
        $page->publish_date=$request->get('publish_date');
        $page->content=$request->get('content');
        $page->update();
        return redirect(route('admins.tables_data'));
    }
    public function destroy(Page $page)
    {
        //dd($page->title);
        $page->delete();
        return redirect(route('admins.tables_data'));
    }
    public function forms()
    {
        return view('admins.forms');
    }
    public function register()
    {
        return view('admins.register');
    }
    public function login()
    {
        return view('admins.login');
    }
    public function tables_data()
    {
        // return view('admins.tables_data');
        $pages = Page::get();
        return view('admins.tables_data',[
            'pages'=>$pages
                ]);
    }
   
}
