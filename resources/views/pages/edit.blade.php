<h1>Create page</h1>
<form action="/pages/edit" method="POST">
    @csrf
    @method('PUT')
    <p>
        <label for="title">Title:</label>
        <input type="text" name="title" id="title">
    </p>
    <p>
        <label for="subtitle">Sub Title:</label>
        <input type="text" name="subtitle" id="subtitle">
    </p>
    <p>
        <label for="author">Author:</label>
        <input type="text" name="author" id="author">
    </p>
    <p>
        <label for="publish_date">Publish Date:</label>
        <input type="date" name="publish_date" id="publish_date">
    </p>
    <p>
        <label for="content">Content:</label>
        <textarea name="content" id="content"></textarea>
    </p>
    <p>
        <button type="submit">Save</button>
    </p>
</form>